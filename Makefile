CC=gcc
CFLAGS=-lsodium -lm

.PHONY: default
default: class-randomizer-1

%: %.c
	$(CC) -o $@ $< $(CFLAGS)

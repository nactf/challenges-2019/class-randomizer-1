FROM gcc:8 as builder

RUN apt-get update && \
   apt-get install -y libsodium-dev && \
   rm -rf /var/lib/apt/lists/*

COPY . /build/
WORKDIR /build/
RUN make

FROM debian:buster

RUN apt-get update && \
  apt-get install -y socat libsodium23 && \
  rm -rf /var/lib/apt/lists/*

COPY --from=builder /build/class-randomizer-1 ./
COPY flag.txt ./

EXPOSE 12345
CMD socat TCP-LISTEN:12345,reuseaddr,fork EXEC:./class-randomizer-1
